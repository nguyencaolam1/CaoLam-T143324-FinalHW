﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmFind
{
    public partial class frmAdd : Form
    {
        public frmAdd()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmAdd_Load);
            this.btnSave.Click += new EventHandler(btnSave_Click);
        }
        void frmAdd_Load(object sender, EventArgs e)
        {
            DBHWEntities db = new DBHWEntities();

        }
        void btnSave_Click(object sender, EventArgs e)
        {
           

          //      MessageBox.Show("You should fill all information!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);
   
                string name = this.txtName.Text;
                string location = this.txtLocation.Text;
                if (name != null || location != null)
                {
                    DBHWEntities db = new DBHWEntities();
                    ITEM item = new ITEM();
                    item.NAME_OF_ITEM = name;
                    item.LOCATION = location;
                    db.ITEMS.Add(item);
                    db.SaveChanges();
                    this.Close();
                    MessageBox.Show("Add item successfully !", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                      
                }
                else
                {
                    MessageBox.Show("You should fill all information!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            

        }
    }
}
