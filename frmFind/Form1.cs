﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace frmFind
{
    public partial class frmFind : Form
    {
        DBHWEntities db = null;
        public frmFind()
        {
            InitializeComponent();
            this.Load += new EventHandler(frmFind_Load);
        }
        void frmFind_Load(object sender, EventArgs e)
        {
            this.LoadItemList();
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
            this.btnUpdate.Click += new EventHandler(btnUpdate_Click);
           // this.btnFind.Click += new EventHandler(btnFind_Click);
            this.btnDelete.Click += new EventHandler(btnDelete_Click);
        }
        public void LoadItemList()
        {
            // for loading tiem from database
            db = new DBHWEntities();
            this.ListItem.DataSource = db.ITEMS.ToList();
          //  this.ListItem.Columns["LOCATION"].Visible = false;
        }
        void btnAdd_Click(object sender, EventArgs e)
        {
            //for add student
            frmAdd add = new frmAdd();
            add.ShowDialog();
            this.LoadItemList();
        }
        void btnUpdate_Click(object sender, EventArgs e)
        {
            //for update location of item
            if (ListItem.SelectedRows.Count == 1)
            {
                var row = ListItem.SelectedRows[0];
                var cell = row.Cells["ID"];
                int id = (int)cell.Value;
                frmUpdate Update = new frmUpdate(id);
                Update.ShowDialog();
                this.LoadItemList();
            }
            else
            {
                MessageBox.Show("You should select a item!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
       // void btnFind_Click(object sender, EventArgs e)
        //{
          //  FindList();
            

         
        //}
        void btnDelete_Click(object sender, EventArgs e)
        {
            if (ListItem.SelectedRows.Count == 1)
            {
                var row= ListItem.SelectedRows[0];
                var cell = row.Cells["ID"];
                int id = (int)cell.Value;
                DBHWEntities db = new DBHWEntities();
                ITEM item = db.ITEMS.Single(st => st.ID == id);
                db.ITEMS.Remove(item);
                db.SaveChanges(); 
                this.LoadItemList();


            }
            else
            {
                MessageBox.Show("You should select a item!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void FindList()
        {
            ListItem.DataSource = db.ITEMS.Where(i => i.NAME_OF_ITEM.Contains(txtName.Text)).ToList();
            
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            FindList();
        }
        
    }
}
