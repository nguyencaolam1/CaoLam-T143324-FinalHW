﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmFind
{
    public partial class frmUpdate : Form
    {
        public frmUpdate(int id)
        {
            InitializeComponent();
            this.Load += new EventHandler(frmUpdate_Load);
            this.btnSave.Click += new EventHandler(btnSave_Click);
            DBHWEntities db = new DBHWEntities();
            item = db.ITEMS.Single(st => st.ID == id);

        }
        private ITEM item;
        void frmUpdate_Load(object sender, EventArgs e)
        {
           
            this.txtID.Text = item.ID.ToString();
            this.txtLocation.Text = item.LOCATION;



        }
        void btnSave_Click(object sender, EventArgs e)
        {
            if (txtLocation != null)
            {
                int id = int.Parse(txtID.Text);
                string location = this.txtLocation.Text;
                item.ID = id;
                item.LOCATION = location;
                DBHWEntities db = new DBHWEntities();
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                this.Close();
                MessageBox.Show("Edit location successfully!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("You need to type new Location!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }
      


    }
}
